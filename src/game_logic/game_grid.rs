use super::extra::{ColorAlpha, ColoredPoint, DIRECTIONS};
use super::tile::{MultiTile};

use super::point::{Point, PointI};
use super::grid::Grid;
use std::collections::HashSet;
use crate::game_logic::point::PointU;

// Superset of grid type with tile support
pub type GameGrid = Grid<ColorAlpha>;
impl GameGrid {
    pub fn insert_tile(&mut self, tile: MultiTile) {
        tile.absolute_points().iter().for_each(|p| self.insert(p.col, p.pos))
    }

    // If space is occupied, or position is outside of edges, return true
    // This is for collision with grid walls
    pub fn does_collide(&self, pos: PointI) -> bool {
        self.out_of_bounds(pos) | self.peek(pos).is_some()
    }

    pub fn absolute_points(&self) -> Vec<ColoredPoint> {
        let mut points: Vec<ColoredPoint> = Vec::new();
        for x in 0..self.width() {
            for y in 0..self.height() {
                let pos: PointI = Point::new(x, y).into();
                if let Some(col) = self.peek(pos) {
                    points.push(pos.color_alpha(*col));
                }
            }
        }
        points
    }

    pub fn find_full_row(&self) -> Option<usize> {
        for y in 0..self.height() {
            let mut count = 0;
            for x in 0..self.width() {
                if let Some(_) = self.peek(Point::new(x, y).into()) {
                    count += 1;
                }
            }
            if count == self.width() {
                return Some(y)
            }
        }
        None
    }

    // Flood fill connected points
    pub fn gather_connected_points(&self, start_point: PointI) -> Vec<ColoredPoint> {
        let start_color = self.peek(start_point);
        if let None = start_color { return vec![] }
        let start_color = *start_color.unwrap();

        let mut point_stack: Vec<PointI> = vec![start_point];
        let mut visited_points: HashSet<PointI> = HashSet::new();

        while let Some(curr_point) = point_stack.pop() {
            if let Some(&col) = self.peek(curr_point) {
                if start_color == col {
                    if let None = visited_points.get(&curr_point) {
                        visited_points.insert(curr_point);
                        for dir in DIRECTIONS.iter() {
                            point_stack.push(curr_point + dir.dir());
                        }
                    }
                }
            }
        }
        visited_points.into_iter()
            .map(|pos| ColoredPoint { pos, col: *self.peek(pos).unwrap() })
            .collect()
    }


    // Add a border to me!
    pub fn border(&mut self, border_color: ColorAlpha) {
        for x in 0..self.width() {
            self.insert(border_color, PointU::new(x, 0usize).into());
            self.insert(border_color, PointU::new(x, self.height() - 1).into());
        }
        for y in 0..self.height() {
            self.insert(border_color, PointU::new(0, y).into());
            self.insert(border_color, PointU::new(self.width() - 1, y).into());

        }
    }

}
