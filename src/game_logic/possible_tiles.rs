use super::tile::{MultiTile};
use super::extra::{Color};
use super::point::{Point, PointI};

pub const POSSIBLE_TILES_COUNT: usize = 7;

lazy_static! {
    pub static ref POSSIBLE_TILES: [MultiTile; POSSIBLE_TILES_COUNT] = {
        let tile_t = MultiTile::new_multi(
            vec![
                PointI::new(0, 0).color(Color::Red),
                PointI::new(1, 0).color(Color::Green),
                PointI::new(2, 0).color(Color::Blue),
                PointI::new(1, 1).color(Color::Blue)
            ],
            Point::zero(),
            (1.0, 0.0).into(),
        );

        let tile_l_left = MultiTile::new_single(
            vec![
                (0, 0).into(),
                (1, 0).into(),
                (2, 0).into(),
                (2, 1).into()],
            Color::Yellow.to_opaque(),
            Point::zero(),
            (1.0, 0.0).into(),
        );
        let tile_t_right = tile_l_left.clone().mirror().change_color(Some(Color::Blue), true);

        let tile_bar = MultiTile::new_multi(
            vec![
                PointI::new(0, 0).color(Color::Red),
                PointI::new(1, 0).color(Color::Yellow),
                PointI::new(2, 0).color(Color::Green),
                PointI::new(3, 0).color(Color::Blue)],
            Point::zero(),
            (1.5, 0.0).into()
        );

       let tile_s = MultiTile::new_multi(
            vec![
                PointI::new(0, 0).color(Color::Red),
                PointI::new(1, 0).color(Color::Yellow),
                PointI::new(1, 1).color(Color::Red),
                PointI::new(2, 1).color(Color::Red)],
            PointI::zero(),
            (1.0, 0.0).into(),
       );

       let tile_s_right = tile_s.clone().mirror().change_color(Some(Color::Green), true);

       let tile_o = MultiTile::new_multi(
            vec![
                PointI::new(0, 0).color(Color::Yellow),
                PointI::new(1, 0).color(Color::Red),
                PointI::new(0, 1).color(Color::Red),
                PointI::new(1, 1).color(Color::Red),
            ],
            PointI::zero(),
            (0.5, 0.5).into(),
       );

       [tile_t, tile_l_left, tile_t_right, tile_bar, tile_s, tile_s_right, tile_o]
    };
}


