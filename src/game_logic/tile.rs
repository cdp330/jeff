use super::extra::*;
use super::game_grid::*;
use super::point::{PointI, PointF};
use std::result::Result::Ok;


#[derive(Clone)]
pub struct MultiTile {
    points: Vec<PointI>,
    colors: Vec<ColorAlpha>,
    pub offset: PointI,
    pub rotation_center: PointF,
}


impl MultiTile {
    pub fn new_multi(points: Vec<ColoredPoint>, offset: PointI, rotation_center: PointF) -> Self {
        MultiTile {
            points: points.iter().map(|p| p.pos).collect(),
            colors: points.iter().map(|p| p.col).collect(),
            offset,
            rotation_center,

        }
    }

    pub fn new_single(points: Vec<PointI>, color: ColorAlpha, offset: PointI, rotation_center: PointF) -> Self {
        MultiTile {
            colors: vec![color; points.len()],
            points,
            offset,
            rotation_center,

        }
    }

    pub fn change_color(mut self, col: Option<Color>, opaque: bool) -> Self {
        for c in self.colors.iter_mut() {
            if let Some(color) = col {
                c.color = color;
            }
            c.opaque = opaque;
        }
        self
    }

    // Mirror the tile along the Y axis (used for initialization)
    pub fn mirror(mut self) -> Self {
        self.points = self.points.iter().map(|p| (p.x * -1, p.y).into()).collect::<Vec<_>>();
        self.rotation_center.x *= -1.0;
        self
    }

    // Return list of which points are used and what their colors are
    pub fn absolute_points(&self ) -> Vec<ColoredPoint> {
        self.points.iter()
            .map(|p| *p + self.offset)
            .zip(self.colors.iter())
            .map(|p| ColoredPoint { pos: p.0, col: *p.1 })
            .collect()
    }

    fn does_intersect_grid(&self, grid: &GameGrid) -> bool {
        self.absolute_points().iter().any(|p| grid.does_collide(p.pos))
    }

    pub fn translate(&mut self, grid: &GameGrid, dir: Directions) -> Result<(), ()> {
        self.offset = self.offset + dir.dir();

        if self.does_intersect_grid(grid) {
            self.offset = self.offset + dir.dir() * -1;

            return Err(())
        }

        Ok(())
    }

    pub fn drop_down(&mut self, grid: &GameGrid) {
        while let Ok(_) = self.translate(grid, Directions::Down){}
    }

    // Rotate tile (performing wall kicks if necessary)
    pub fn rotate(&mut self, grid: &GameGrid) {

        // Stores data for unbudging in a direction
        struct UnbudgingRecord {
            dir: Directions, // Direction we're unbudging in
            is_bordered: bool, // Does this tile have space to unbudge in this direction?
        };

        let unbudges: Vec<UnbudgingRecord> = [Directions::Up, Directions::Left, Directions::Right].iter().map(|d| {
           UnbudgingRecord {
               dir: *d,
               is_bordered: self.absolute_points().iter().any(|p| grid.does_collide(p.pos + d.dir())),
           }
        }).collect();


        let old_points = self.points.clone();

        // Perform actual rotation
        self.points = self.points.iter().map(|p| {
            let mut centered_around_zero = PointF::from(*p) + self.rotation_center * -1.0;
            centered_around_zero = PointF::new(-centered_around_zero.y, centered_around_zero.x);
            (centered_around_zero + self.rotation_center).round()
        }).collect();

        // Attempt unbudging
        if self.does_intersect_grid(grid) {
            for record in unbudges {
                if !record.is_bordered {
                    for _ in 0..2 {
                        self.offset = self.offset + record.dir.dir();
                        if !self.does_intersect_grid(grid) {
                            return;
                        }
                    }
                }
            }

            //     // If collision resolution unsuccessful, undo the rotation
            self.points = old_points;

        }
    }

}

