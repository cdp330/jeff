use super::point::{Point, PointI};
use std::fmt;
use std::fmt::{Formatter, Error};


#[derive(Clone)]
pub struct Grid<T> {
    width: usize,
    height: usize,
    full_slots: usize,
    data: Box<[Option<T>]>,
}


impl<T: Clone> Grid<T> {

    pub fn new(width: usize, height: usize) -> Self {
        Grid {
            width,
            height,
            full_slots: 0,
            data: vec![None; width * height].into_boxed_slice()
        }
    }

    pub fn insert(&mut self, item: T, pos: PointI) {
        if let Ok(index) = self.get_index(pos) {
            if let None = self.data[index] {
                self.full_slots += 1;
            }
            self.data[index] = Some(item);
        }
    }

    pub fn remove(&mut self, pos: PointI) -> Option<T> {
        if let Ok(index) = self.get_index(pos) {
            self.full_slots -= 1;
            return self.data[index].take();
        }
        None
    }
}

impl<T> Grid<T> {
    pub fn peek(&self, pos: PointI) -> Option<&T> {
        if let Ok(index) = self.get_index(pos) {
            return self.data[index].as_ref();
        }
        None
    }

    pub fn out_of_bounds(&self, pos: PointI) -> bool {
        pos.x < 0 || pos.y < 0 || pos.x >= self.width as i32 || pos.y >= self.height as i32
    }

    // Gives OK and data index if pos is within grid, err otherwise
    fn get_index(&self, pos: PointI) -> Result<usize, ()> {
        if self.out_of_bounds(pos) {
            return Err(())
        }
        Ok(pos.x as usize + (pos.y as usize * self.width))
    }

    pub const fn width(&self) -> usize { self.width }
    pub const fn height(&self) -> usize { self.height }
    pub const fn full_slots(&self) -> usize { self.full_slots }
}

// Draw the grid out in 2D
impl<T> fmt::Debug for Grid<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        for y in 0..self.height {
            write!(f, "\r\n")?;
            for x in 0..self.width {
                match self.peek(Point::new(x, y).into()) {
                    Some(_) => write!(f, "██")?,
                    None => write!(f, "  ")?,
                }
            }
        }
        write!(f, "\r\n")?;
        Ok(())
    }
}

#[cfg(test)]
mod grid_tests {
    use crate::game_logic::grid::Grid;

    #[test]
    fn test_1() {

        #[derive(Clone, Debug)]
        struct Object;
        let mut grid_1: Grid<Object> = Grid::new(4, 4);
        grid_1.insert(Object{}, (1, 1).into());

        assert!(grid_1.peek((2, 2).into()).is_none());
        assert!(grid_1.peek((1, 1).into()).is_some());

        let item = grid_1.remove((1, 1).into());
        assert!(item.is_some());
        assert!(grid_1.peek((1, 1).into()).is_none());

        println!("{:?}", grid_1);
    }
}

