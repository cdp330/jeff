pub mod state_gameover;
pub mod state_menu;
pub mod state_playing;
pub mod state_falling;


use super::game_grid::{GameGrid};
use enum_dispatch::enum_dispatch;

use state_playing::StatePlaying;
use state_falling::StateFalling;
use state_menu::StateMenu;
use crate::game_logic::game_states::state_gameover::StateGameOver;

#[enum_dispatch]
pub enum GameState {
    StatePlaying(StatePlaying),
    StateFalling(StateFalling),
    StateMenu(StateMenu),
    StateGameOver(StateGameOver)
}

#[enum_dispatch(GameState)]
pub trait GameStateBehaviours {

    // Update and potentially change state
    fn update(self) -> GameState;

    // Flatten state into a grid so it can be rendered
    fn flatten(&self) -> GameGrid;
}




