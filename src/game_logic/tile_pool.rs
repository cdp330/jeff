use super::tile::MultiTile;
use crate::game_logic::possible_tiles::POSSIBLE_TILES_COUNT;
use crate::game_logic::possible_tiles::POSSIBLE_TILES;

use rand::seq::SliceRandom;
use crate::game_logic::game_grid::GameGrid;
use crate::game_logic::point::{Point, PointI, PointU};
use crate::game_logic::extra::Color;


struct TileSet([MultiTile; POSSIBLE_TILES_COUNT]);

impl TileSet {
    fn new() -> Self {
        let mut pool: [MultiTile; POSSIBLE_TILES_COUNT] = POSSIBLE_TILES.clone();
        pool.shuffle(&mut rand::thread_rng());
        TileSet(pool)
    }
}



// "bag" to pull random tiles out of. All 7 tiles are guaranteed to be in this bag to prevent tile drought.
pub struct TilePool {
    pool_1: TileSet,
    pool_2: TileSet,
    curr_index: usize,
}


impl TilePool {
    pub fn new() -> Self {
        TilePool { pool_1: TileSet::new(), pool_2: TileSet::new(), curr_index: 0 }
    }


    pub fn take_tile(&mut self) -> MultiTile {
        let tile = self.pool_1.0[self.curr_index].clone();
        self.curr_index += 1;
        if self.curr_index >= self.pool_1.0.len() {
            self.curr_index = 0;

            // Swap pools
            self.pool_1 = std::mem::replace(&mut self.pool_2, TileSet::new());
        }
        tile
    }


    fn iter(&self) -> TilePoolIter {
        TilePoolIter{ pool: self, iter_index: self.curr_index }
    }


    // Display next 3 upcoming tiles in a grid
    pub fn render(&self) -> GameGrid {
        let mut grid = GameGrid::new(10, 14);
        grid.border(Color::Yellow.to_opaque());

        let mut tile_iter = self.iter();

        for i in 0..3 {
            if let Some(tile) = tile_iter.next() {

                for p in tile.absolute_points() {
                    // Stagger the tiles so they don't overlap
                    grid.insert(p.col, p.pos + (4, 2 + (4 * i)).into() + tile.offset * -1);
                }
            }
        }

        grid

    }

    // Swap next piece with a provided one...
    pub fn swap(&mut self, to_swap: MultiTile) -> MultiTile {
        std::mem::replace(&mut self.pool_1.0[self.curr_index], to_swap)
    }
}


struct TilePoolIter<'a> {
    pool: &'a TilePool,
    iter_index: usize
}

impl<'a> Iterator for TilePoolIter<'a> {
    type Item = &'a MultiTile;

    fn next(&mut self) -> Option<Self::Item> {
        let item = if self.iter_index >= POSSIBLE_TILES_COUNT {
            self.pool.pool_1.0.get(self.iter_index - POSSIBLE_TILES_COUNT)
        }
        else {
            self.pool.pool_1.0.get(self.iter_index)
        };

        self.iter_index += 1;
        item
    }
}




#[cfg(test)]
mod tile_pool_tests {
    use crate::game_logic::tile_pool::*;

    #[test]
    fn test_1() {
        let pool = TilePool::new();





    }
}