
use super::point::{PointI};

#[derive(Copy, Clone)]
pub struct ColoredPoint {
    pub pos: PointI,
    pub col: ColorAlpha,
}

// Color with transparency
#[derive(Copy, Clone, PartialEq)]
pub struct ColorAlpha { pub color: Color, pub opaque : bool, }



#[derive(Copy, Clone, PartialEq)]
pub enum Color { Red, Blue, Green, Yellow, }
pub static COLORS: &[Color] = &[Color::Red, Color::Yellow, Color::Green, Color::Blue];

impl Color {
    pub const fn to_opaque(&self) -> ColorAlpha {
        ColorAlpha { color: *self, opaque: true}
    }
    pub const fn to_transp(&self) -> ColorAlpha { ColorAlpha { color: *self, opaque: false} }
}

#[derive(Copy, Clone)]
pub enum Directions {
    Left, Right, Down, Up
}
pub static DIRECTIONS: &[Directions] = &[Directions::Left, Directions::Right, Directions::Up, Directions::Down];

impl Directions {
    pub fn dir(self) -> PointI {
        match self {
            Directions::Left => (-1, 0).into(),
            Directions::Right => (1, 0).into(),
            Directions::Down => (0, 1).into(),
            Directions::Up => (0, -1).into(),
        }
    }
}

// Represents a countdown / timer thingy
#[derive(Clone)]
pub struct Countdown { lifetime: u32, max_lifetime: u32 }
impl Countdown {
    pub fn new(lifetime: u32) -> Self {
        Countdown { lifetime, max_lifetime: lifetime }
    }

    pub fn remaining(&self) -> u32 {
        self.lifetime
    }

    // Return OK if decremented successfully, Err if lifetime reached zero
    pub fn decrement(&mut self) -> Result<(), ()> {
        return if self.lifetime > 0 {
            self.lifetime -= 1;
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn has_elapsed(&self) -> bool {
        self.lifetime == 0
    }

    pub fn zero(&mut self) {
        self.lifetime = 0;
    }

    pub fn reset(&mut self) {
        self.lifetime = self.max_lifetime;
    }
}
