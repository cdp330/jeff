use std::ops::{Add, Mul};
use super::extra::{ColoredPoint, Color, ColorAlpha};

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Point<T> { pub x: T, pub y: T }

pub type PointU = Point<usize>;
pub type PointI = Point<i32>;
pub type PointF = Point<f32>;

// Constructors
impl<T> Point<T> {
    pub const fn new(x: T, y: T) -> Point<T> {
        Point {x, y}
    }
}

impl<T> From<(T, T)> for Point<T> {
    fn from(tuple: (T, T)) -> Self {
        Point::new(tuple.0, tuple.1)
    }
}

impl<T: From<i8>> Point<T> {
    pub fn zero() -> Point<T> {
        Point::new(T::from(0), T::from(0))
    }
}

// Conversions to other points
impl PointF {
    pub fn round(self) -> PointI {
        Point::new(self.x.floor() as i32, self.y.floor() as i32)
    }
}

impl From<PointU> for PointI {
    fn from(source: PointU) -> Self { Point::new(source.x as i32, source.y as i32) }
}

impl From<PointI> for PointU {
    fn from(source: PointI) -> Self { Point::new(source.x as usize, source.y as usize) }
}

impl From<PointF> for PointI {
    fn from(source: PointF) -> Self { Point::new(source.x as i32, source.y as i32) }
}


impl From<PointI> for PointF {
    fn from(source: PointI) -> Self { Point::new(source.x as f32, source.y as f32) }
}

// Conversions to other types
impl PointI {
    pub const fn color(self, col: Color) -> ColoredPoint {
        ColoredPoint { pos: self, col: col.to_opaque() }
    }

    pub const fn color_alpha(self, col: ColorAlpha) -> ColoredPoint {
        ColoredPoint { pos: self, col }
    }
}

// Operations
impl<T: Mul<Output=T> + Copy> Mul<T> for Point<T> {
    type Output = Self;
    fn mul(self, rhs: T) -> Self::Output {
        Point { x: self.x * rhs, y: self.y * rhs }
    }
}
impl<T: Add<Output=T>> Add for Point<T> {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Point { x: self.x + rhs.x, y: self.y + rhs.y }
    }
}




#[cfg(test)]
mod point_tests {
    use super::*;

    #[test]
    fn test_1() {
        // Can create point from tuple
        let p: Point<i32> = (3, 3).into();
        assert_eq!(p, PointI::new(3, 3));
        assert_eq!(Point::new(3.0, 4.0), Point::new(-3.0, -4.0) * -1.0);
    }
}

