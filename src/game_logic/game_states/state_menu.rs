use crate::game_logic::game_grid::GameGrid;

use crate::game_logic::game_states::GameStateBehaviours;
use crate::game_logic::game_states::GameState;
use crate::game_logic::game_states::state_falling::StateFalling;
use crate::game_logic::extra::Color;
use crate::game_logic::game_states::state_playing::StatePlaying;



pub struct StateMenu {
    background_scene: Option<StateFalling>, // Some when falling is still occurring, None when falling has ended
    flattened_grid: GameGrid,

    pub start_game: bool
}

impl StateMenu {
    pub fn new() -> Self {
        let mut grid = GameGrid::new(20, 10);
        // Insert tiles to form game title on fall
        // J

        grid.insert(Color::Green.to_opaque(), (2, 5).into());
        grid.insert(Color::Green.to_opaque(), (3, 5).into());
        grid.insert(Color::Green.to_opaque(), (4, 5).into());
        grid.insert(Color::Green.to_opaque(), (2, 0).into());

        grid.insert(Color::Yellow.to_opaque(), (4, 0).into());
        grid.insert(Color::Yellow.to_opaque(), (4, 2).into());
        grid.insert(Color::Yellow.to_opaque(), (4, 3).into());
        grid.insert(Color::Yellow.to_opaque(), (4, 4).into());


        // E
        grid.insert(Color::Yellow.to_opaque(), (6, 0).into());
        grid.insert(Color::Yellow.to_opaque(), (7, 7).into());

        grid.insert(Color::Red.to_opaque(), (6, 3).into());
        grid.insert(Color::Yellow.to_opaque(), (6, 4).into());
        grid.insert(Color::Yellow.to_opaque(), (7, 4).into());

        grid.insert(Color::Red.to_opaque(), (6, 8).into());
        grid.insert(Color::Red.to_opaque(), (6, 9).into());

        grid.insert(Color::Yellow.to_opaque(), (7, 0).into());
        grid.insert(Color::Yellow.to_opaque(), (8, 0).into());

        grid.insert(Color::Red.to_opaque(), (8, 1).into());

        // F

        grid.insert(Color::Green.to_opaque(), (10, 3).into());
        grid.insert(Color::Blue.to_opaque(), (10, 4).into());
        grid.insert(Color::Blue.to_opaque(), (11, 4).into());
        grid.insert(Color::Blue.to_opaque(), (12, 4).into());


        grid.insert(Color::Green.to_opaque(), (10, 8).into());
        grid.insert(Color::Green.to_opaque(), (10, 9).into());

        grid.insert(Color::Blue.to_opaque(), (10, 0).into());

        grid.insert(Color::Blue.to_opaque(), (11, 0).into());
        grid.insert(Color::Blue.to_opaque(), (12, 0).into());


        // F

        // grid.insert(Color::Green.to_opaque(), (14, 3).into());
        grid.insert(Color::Blue.to_opaque(), (14, 2).into());
        grid.insert(Color::Blue.to_opaque(), (15, 2).into());
        grid.insert(Color::Blue.to_opaque(), (16, 2).into());


        grid.insert(Color::Green.to_opaque(), (14, 6).into());
        grid.insert(Color::Green.to_opaque(), (14, 7).into());

        grid.insert(Color::Blue.to_opaque(), (14, 1).into());
        grid.insert(Color::Blue.to_opaque(), (14, 0).into());
        grid.insert(Color::Blue.to_opaque(), (15, 0).into());
        grid.insert(Color::Blue.to_opaque(), (16, 0).into());

        let background_scene = StateFalling::new(grid);
        let flattened_grid = background_scene.flatten();

        StateMenu { background_scene: Some(background_scene), flattened_grid, start_game: false }
    }
}

impl GameStateBehaviours for StateMenu {
    fn update(mut self) -> GameState {

        if self.start_game {
            return StatePlaying::new(GameGrid::new(12, 28)).into();
        }

        if let Some(background_scene) = self.background_scene {
            self.flattened_grid = background_scene.flatten();

            if let GameState::StateFalling(falling) = background_scene.update() {
                self.background_scene = Some(falling);
            }
            else {
                self.background_scene = None;
            }
        }

        self.into()
    }

    fn flatten(&self) -> GameGrid {
        self.flattened_grid.clone()
    }
}