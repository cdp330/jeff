use crate::game_logic::tile::{MultiTile};
use crate::game_logic::game_grid::{GameGrid};
use crate::game_logic::extra::{Directions, Countdown};
use crate::game_logic::possible_tiles::POSSIBLE_TILES;

use std::mem;

use crate::game_logic::game_states::{GameStateBehaviours, GameState};
use crate::game_logic::game_states::state_falling::StateFalling;
use crate::game_logic::point::{Point, PointU};
use crate::game_logic::tile_pool::TilePool;
use crate::game_logic::game_states::state_gameover::StateGameOver;

pub struct StatePlaying {
    current_tile : MultiTile,
    grid : GameGrid,
    pub paused: bool,
    pub pool: TilePool,

    // Decrements if tile is blocked while moving downward. Is reset upon a successful downwards movement.
    // To coordinate auto merging tiles with grid on update.
    pub curr_tile_lifetime: Countdown,
}

pub static DEFAULT_TILE_LIFETIME: u32 = 4;

impl GameStateBehaviours for StatePlaying {
    fn update(mut self) -> GameState {
        if !self.paused {
            if let Err(_) = self.current_tile.translate(&self.grid, Directions::Down) {

                // Only merge tile if its lifetime runs out!
                if let Err(_) = self.curr_tile_lifetime.decrement() {
                    let new_tile = self.pick_new_tile();
                    let old_tile = mem::replace(&mut self.current_tile, new_tile);
                    self.grid.insert_tile(old_tile);

                    // If we had a full row, remove it and make all tiles fall
                    if let Some(row) = self.grid.find_full_row() {
                        for x in 0..self.grid.width() {
                            self.grid.remove(Point::new(x, row).into());
                        }
                        return StateFalling::new(self.grid).into()
                    }
                }


            }
            else {
                self.curr_tile_lifetime.reset();
            }
        }

        // Detect if player filled top row up
        if self.top_row_filled() {
            return StateGameOver::new().into();
        }

        self.into()
    }



    fn flatten(&self) -> GameGrid {
        let mut grid_copied = self.grid.clone();

        let mut ghost_tile = self.current_tile.clone().change_color(None, false);
        ghost_tile.drop_down(&self.grid);

        grid_copied.insert_tile(ghost_tile);
        grid_copied.insert_tile(self.current_tile.clone());


        grid_copied
    }
}

impl StatePlaying {

    pub fn new(grid: GameGrid) -> Self {
        let mut game = StatePlaying {
            current_tile: POSSIBLE_TILES[0].clone(),
            grid,
            paused: false,
            pool: TilePool::new(),

            curr_tile_lifetime: Countdown::new(DEFAULT_TILE_LIFETIME)
        };
        game.current_tile = game.pick_new_tile();
        game
    }

    fn top_row_filled(&self) -> bool {
        for x in 0..self.grid.width() {
            if let Some(_) = self.grid.peek(PointU::new(x, 4).into()) {
                return true
            }
        }
        false
    }

    pub fn translate_current(&mut self, dir: Directions) {
        if !self.curr_tile_lifetime.has_elapsed() {
            let _ = self.current_tile.translate(&self.grid, dir);
        }
    }

    pub fn rotate_current(&mut self) {
        self.current_tile.rotate(&self.grid);
    }

    pub fn drop_current(&mut self) {
        self.current_tile.drop_down(&self.grid);
    }


    fn pick_new_tile(&mut self) -> MultiTile {
        let mut new_tile = self.pool.take_tile();
        new_tile = self.position_tile(new_tile);
        new_tile
    }

    // Zero tile's position
    fn position_tile(&self, mut tile: MultiTile) -> MultiTile {
        tile.offset.x = (self.grid.width() / 2) as i32;
        tile.offset.y = 2;
        tile
    }

    // Swap current piece with next
    pub fn stash_current(&mut self) {
        let mut new_tile = self.pool.swap(self.current_tile.clone());
        new_tile = self.position_tile(new_tile);
        self.current_tile = new_tile;
    }


}