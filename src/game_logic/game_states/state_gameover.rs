use crate::game_logic::extra::Countdown;
use crate::game_logic::game_states::{GameStateBehaviours, GameState};
use crate::game_logic::game_grid::GameGrid;
use crate::game_logic::game_states::StateMenu;

pub struct StateGameOver {
    pub countdown: Countdown,
}
impl StateGameOver {
    pub fn new() -> Self {
        StateGameOver{ countdown: Countdown::new(12) }
    }
}

impl GameStateBehaviours for StateGameOver {
    fn update(mut self) -> GameState {
        match self.countdown.decrement() {
            Ok(_) => self.into(),
            Err(_) => StateMenu::new().into(),
        }
    }

    fn flatten(&self) -> GameGrid {
        GameGrid::new(0,0)
    }
}