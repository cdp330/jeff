use crate::game_logic::game_states::{GameStateBehaviours, GameState};
use crate::game_logic::grid::Grid;
use crate::game_logic::extra::{ColorAlpha, ColoredPoint, Directions};
use crate::game_logic::point::Point;
use crate::game_logic::tile::MultiTile;
use crate::game_logic::game_grid::GameGrid;
use crate::game_logic::game_states::state_playing::StatePlaying;
use std::option::Option::Some;

pub struct StateFalling {
    loose_tiles: Vec<MultiTile>,
    grid: GameGrid
}

impl GameStateBehaviours for StateFalling {
    fn update(mut self) -> GameState {
        if self.loose_tiles.is_empty() {

            // Double check if we have a full row
            if let Some(row) = self.grid.find_full_row() {
                for x in 0..self.grid.width() {
                    self.grid.remove(Point::new(x, row).into());
                }
                return Self::new(self.grid).into()
            }
            return StatePlaying::new(self.grid).into()
        }
        else {
            loop {
                // Translate all tiles down, and split into two categories: blocked on translate, and free
                let grid = &self.grid;
                let (blocked, free_falling): (Vec<_>, Vec<_>) = self.loose_tiles.into_iter()
                    .map(|mut t| (t.translate(grid, Directions::Down), t))
                    .partition(|t| t.0.is_err());

                // Free falling goes back into regular loose tiles
                self.loose_tiles = free_falling.into_iter().map(|t| t.1).collect();

                // If no blocked tiles, exit the loop early (we have achieved our job of moving some tiles down)
                if blocked.is_empty() {
                    break;
                }

                // If there were blocked tiles, shift free tiles up (because they could overlap with the blocked tiles...)
                // Continue the loop until a tile visibly moves down!
                self.loose_tiles = self.loose_tiles.into_iter()
                    .map(|mut t| {
                        let _ = t.translate(grid, Directions::Up);
                        t
                    }).collect();

                // Insert blocked tiles into grid
                blocked.into_iter().map(|t| t.1).for_each(|t| {
                    self.grid.insert_tile(t);
                });
            }
        }
        self.into()
    }

    fn flatten(&self) -> Grid<ColorAlpha> {
        let mut cloned = self.grid.clone();
        self.loose_tiles.clone().into_iter().for_each(|tile| cloned.insert_tile(tile));
        cloned
    }
}

impl StateFalling {
    pub fn new(mut grid: GameGrid) -> Self {
        // Split grid into connected regions
        let mut split_tiles = vec![];

        while let Some(pos) = grid.absolute_points().pop() {
            let connected: Vec<ColoredPoint> = grid.gather_connected_points(pos.pos);
            connected.iter().for_each(|p| { grid.remove(p.pos); });
            split_tiles.push(MultiTile::new_multi(connected,Point::zero(),Point::zero()));
        }
        StateFalling { loose_tiles: split_tiles, grid }
    }
}