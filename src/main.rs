mod game_logic;

#[macro_use]
extern crate lazy_static;

use std::{
    sync::mpsc::{Sender, Receiver},
    sync::mpsc,
    time::Duration,
    io::{stdout, Stdout}
};
use crate::{
    game_logic::{
        game_states::GameState,
        game_grid::GameGrid,
        game_states::GameStateBehaviours,
        extra::{ColorAlpha, Directions},
        point::Point,
        extra,
    },

};

use crossterm::{
    style::{Print, Color, SetForegroundColor},
    event::{poll, Event, KeyCode},
    ExecutableCommand,
    QueueableCommand,
    terminal::{Clear, ClearType},
    cursor::MoveTo,
    style::SetBackgroundColor
};
use crate::game_logic::game_states::state_menu::StateMenu;
use crate::game_logic::extra::COLORS;


fn tile_to_term_col(col: extra::Color) -> Color {
    match col {
        extra::Color::Red => Color::Red,
        extra::Color::Blue => Color::Blue,
        extra::Color::Green => Color::Green,
        extra::Color::Yellow => Color::Yellow,
    }
}

fn render_game_grid(grid: &GameGrid, stdout: &mut Stdout, offset: Point<u16>) -> crossterm::Result<()> {
    stdout.queue(SetBackgroundColor(Color::Black))?;

    for y in 0..grid.height() {
        stdout.queue(MoveTo(1 + offset.x, offset.y + y as u16))?;
        for x in 0..grid.width() + 1{

            match grid.peek(Point::new(x, y).into()) {
                Some(col) => {
                    stdout
                        .queue(SetForegroundColor(tile_to_term_col(col.color)))?
                        .queue(Print(match col.opaque {
                            true => "██",
                            false => "░░"
                        }))?;

                },
                None => {
                    let spacer = match grid.peek(Point::new(x as i32 - 1, y as i32).into()) {
                        None | Some(ColorAlpha{ opaque : false, .. }) => "  ",
                        _ => "▒ ",
                    };
                    stdout.queue(Print(spacer))?;
                }
            }
        }
    }

    Ok(())
}

fn render_text(text: &str, offset: Point<u16>, color: crossterm::style::Color) -> crossterm::Result<()> {
    stdout()
        .queue(SetForegroundColor(color))?
        .queue(MoveTo(1 + offset.x, 1 + offset.y))?
        .queue(Print(text))?;
    Ok(())
}


fn main() -> Result<(), String> {
    enum GameEvent {
        Update,
        Key(KeyCode)
    };

    // Set up input timing
    let event_receiver = {
        let (event_sender_1, event_receiver): (Sender<GameEvent>, Receiver<GameEvent>) = mpsc::channel();
        let event_sender_2 = event_sender_1.clone();

        // Periodically emit tick event
        std::thread::spawn(move || {
            loop {
                event_sender_1.send(GameEvent::Update).unwrap();
                std::thread::sleep(Duration::from_millis(200));
            }
        });

        // Send input events in parallel
        std::thread::spawn(move || {
            loop {
                if poll(Duration::from_millis(100)).unwrap() {
                    if let Event::Key(e) = crossterm::event::read().unwrap() {
                        let _ = event_sender_2.send(GameEvent::Key(e.code));
                    }
                }
            }
        });

        event_receiver
    };

    let mut stdout = stdout();
    crossterm::terminal::enable_raw_mode().unwrap();
    stdout
        .execute(Clear(ClearType::All)).unwrap()
        .execute(crossterm::cursor::Hide).unwrap();


    let mut game: GameState = StateMenu::new().into();

    loop {
        if let Ok(event) = event_receiver.try_recv() {
            let old_game_state_discriminant = std::mem::discriminant(&game);
            match event {
                GameEvent::Update => {game = game.update()},
                GameEvent::Key(k) => if let KeyCode::Char('q') = k {
                    break
                },
            }

            // After updating, game state might have changed! We want to clear the screen when it changes.
            if std::mem::discriminant(&game) != old_game_state_discriminant {
                stdout.queue(Clear(ClearType::All)).unwrap();
            }



            match &mut game {
                GameState::StateMenu(menu) => {
                    render_text("Press any key to start", (8, 12).into(), Color::Green).unwrap();
                    render_text("Controls:", (8, 14).into(), Color::Yellow).unwrap();
                    render_text("c: stash current, arrows: move, pace: drop, b: pause, q: quit", (8, 15).into(), Color::Yellow).unwrap();


                    // Start game on any keypress
                    if let GameEvent::Key(_) = event {
                        menu.start_game = true;
                    }
                }

                GameState::StatePlaying(playing) => {
                    if let GameEvent::Key(keycode) = event {
                        match keycode {
                            KeyCode::Down => playing.translate_current(Directions::Down),
                            KeyCode::Left => playing.translate_current(Directions::Left),
                            KeyCode::Right => playing.translate_current(Directions::Right),
                            KeyCode::Up => playing.rotate_current(),
                            KeyCode::Char(' ') => {
                                playing.drop_current();

                                playing.curr_tile_lifetime.zero(); // Need to ensure the tile is immediately "stuck"
                            },
                            KeyCode::Char('c') => playing.stash_current(),
                            KeyCode::Char('b') => { playing.paused = !playing.paused },
                            _ => {},
                        }
                    }

                    render_game_grid(&playing.pool.render(), &mut stdout, (26, 0).into()).unwrap();

                },

                GameState::StateGameOver(over) => {
                    let remaining_div = over.countdown.remaining() / 3;
                    let colorcode = *COLORS.get(remaining_div as usize).unwrap_or(&extra::Color::Blue);

                    render_text(
                        &format!("Game over, returning to main menu in {}...", over.countdown.remaining()),
                        (4, 4).into(),
                        tile_to_term_col(colorcode)
                    ).unwrap();
                }

                _ => {}
            }



        }

        render_game_grid(&game.flatten(), &mut stdout, (0, 0).into()).unwrap();
    }


    Ok(())
}