# Jeff
A tetis-like game that runs in Linux and Windows terminals. I programmed this to help myself learn Rust. 

Unlike most implementations of Tetris, tiles in Jeff are subject to gravity upon making a full row, as shown below. Placed tiles are split into connected 'islands' of the same color via a flood fill. This leads to interesting emergent behaviour.

![demo of game running](demo.gif)

# Compiling / Running
You'll need the Rust compiler. Clone the repository and run:
`cargo run --release`

Because this project uses [Crossterm](https://github.com/crossterm-rs/crossterm#tested-terminals) to abstract away ANSI escape codes, it should work on most UNIX and Windows terminals (starting with Windows 7). 